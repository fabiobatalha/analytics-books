# coding: utf-8

import apachelog
from pymongo import MongoClient
import sys
import os
import re
import json
from urlparse import urlparse
from datetime import date
import argparse
import logging
import utils

from choices import *

ROBOTS_FILE = utils.settings['app:main'].get('robots_file', '')
MONGODB_DOMAIN = utils.settings['app:main'].get('mongodb_host', '127.0.0.1:27017').split(":")[0]
MONGODB_PORT = int(utils.settings['app:main'].get('mongodb_host', '127.0.0.1:27017').split(":")[1])
MONGODB_DBNAME = utils.settings['app:main'].get('mongodb_db', 'books')
LOGS_DIR = utils.settings['app:main'].get('logs_dir', '/var/www/analytics-books/data/logs')
ROBOTS = [i.strip() for i in open(ROBOTS_FILE, 'r')]
COMPILED_ROBOTS = [re.compile(i.lower()) for i in ROBOTS]
ALLOWED_PATTERNS = [
    ('pdf', {'index': {'context': 'book'}, 'pattern': re.compile(r'GET \/id\/(?P<sbid>[a-z0-9]+?)\/pdf\/.+?\.pdf HTTP')}),
    ('pdf', {'index': {'context': 'book'}, 'pattern': re.compile(r'GET \/scielobooks\/(?P<sbid>[a-z0-9]+?)\/pdf\/.+?\.pdf HTTP')}),
    ('epub', {'index': {'context': 'book'}, 'pattern': re.compile(r'GET \/id\/(?P<sbid>[a-z0-9]+?)\/epub\/.+?\.epub HTTP')}),
    ('html', {'index': {'context': 'book'}, 'pattern': re.compile(r'GET \/id\/(?P<sbid>[a-z0-9]+?) HTTP')}),
]
APACHE_LOG_FORMAT = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
CONN = MongoClient(MONGODB_DOMAIN, MONGODB_PORT)
DB = CONN[MONGODB_DBNAME + "_accesslog"]
PROC_FILES = DB[MONGODB_DBNAME + "_processed_files"]
ERROR_LOG = DB[MONGODB_DBNAME + "_error_log"]
ANALYTICS = DB[MONGODB_DBNAME + "_analytics"]

logger = logging.getLogger(__name__)


def _config_logging(logging_level='INFO', logging_file=None):

    allowed_levels = {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    logger.setLevel(allowed_levels.get(logging_level, 'INFO'))

    if logging_file:
        hl = logging.FileHandler(logging_file, mode='a')
    else:
        hl = logging.StreamHandler()

    hl.setFormatter(formatter)
    hl.setLevel(allowed_levels.get(logging_level, 'INFO'))

    logger.addHandler(hl)

    return logger


def validate_date(data):
    """
    Simple validate each log line date.
    The year must be minor then the actual year
    The month must be between 1 and 12
    Both must be a valid number
    """
    allowed_month = range(1, 13)
    today_year = date.today().year

    month = ""
    if data['%t'][4:7].upper() in MONTH_DICT:
        month = MONTH_DICT[data['%t'][4:7].upper()]

    dat = data['%t'][8:12]+month

    if len(str(dat)) == 6:
        if int(dat[0:4]) <= int(today_year) and (int(dat[4:6]) in allowed_month):
            return dat

    return False


def is_bot(user_agent):
    """
    Simple validate each log line against the STOP_WORDS defined in the conf
    file.
    """
    for robot in COMPILED_ROBOTS:
        if robot.search(user_agent, re.IGNORECASE):
            return True

    return False


def get_sbid_from_request(request):
    sbid = None
    doc_type3 = None

    for doc_type, pattern in ALLOWED_PATTERNS:
        search_results = pattern['pattern'].search(request)

        if not search_results:
            continue

        sbid = search_results.groupdict().get('sbid', None)
        doc_type3 = doc_type[0:3]

        if sbid:
            return (sbid, doc_type3, pattern)

    return (None, None, None)


def registering_log(request, date_log):
    """
    Register the log access acording to the ALLOWED_PATTERNS
    """
    sbid, doc_type, pattern = get_sbid_from_request(request)

    if not sbid:
        return

    ANALYTICS.update(
        {"code": "website"},
        {
            "$set": {"context": "website"},
            "$inc": {doc_type + '_' + date_log: 1, doc_type + '_total': 1}
        },
        True
    )
    ANALYTICS.update(
        {"code": sbid},
        {"$set": pattern['index'], "$inc": {doc_type: 1, doc_type + '_' + date_log: 1}}, True)


def run():

    parser = apachelog.parser(APACHE_LOG_FORMAT)

    ANALYTICS.ensure_index('code')
    # Creating Index according to Allowed Urls defined in the conf file
    for page, index in ALLOWED_PATTERNS:
        ANALYTICS.ensure_index(page)
        for indexkey in index['index']:
            ANALYTICS.ensure_index(indexkey)

    logger.info("listing log files at: " + LOGS_DIR)
    logfiles = os.popen('ls ' + LOGS_DIR + '/*.log')
    for file in logfiles:
        filepath = file.strip()
        fileloaded = open(filepath, 'r')

        if PROC_FILES.find({'_id': filepath}).count() == 0:
            lines = 0
            lines = os.popen('cat '+filepath+' | wc -l').read().strip()
            PROC_FILES.update(
                {"_id": filepath},
                {'$set': {
                    'proc_date': date.isoformat(date.today()),
                    'status': 'processing',
                    'lines': lines
                }},
                True
            )
            logger.info("processing %s" % filepath)
            linecount = 0
            for line in fileloaded:
                linecount = linecount + 1
                PROC_FILES.update(
                    {"_id": filepath},
                    {'$set': {'line': linecount}},
                    True
                )
                try:
                    data = parser.parse(line)
                except:
                    sys.stderr.write("Unable to parse %s" % line)
                    ANALYTICS.update(
                        {"code": "website"},
                        {
                            "$set": {"context": "website"},
                            "$inc": {'err_total': 1, 'err_unabletoparser': 1}
                        },
                        True
                    )
                    ERROR_LOG.update(
                        {"file": filepath},
                        {"$set": {'lines': linecount}, "$inc": {'err_unabletoparser': 1}},
                        True
                    )
                    continue

                #Validating the log date
                valid_date = validate_date(data)
                if not valid_date:
                    continue

                # Validating Agains Bot List
                if is_bot(data['%{User-Agent}i']):
                    ANALYTICS.update(
                        {"code": "website"},
                        {
                            "$set": {"context": "website"},
                            "$inc": {'bot_' + valid_date: 1, 'bot_total': 1}
                        },
                        True
                    )
                    continue

                # Registering Log
                registering_log(data['%r'], valid_date)

            #Changing the status of the log file to processed after all lines was parsed
            PROC_FILES.update(
                {"_id": filepath},
                {'$set': {'status': 'processed'}},
                True
            )
        else:
            logger.info("%s was already processed" % filepath)


def main():

    parser = argparse.ArgumentParser(
        description='Read apache log files and load access data to MongoDB'
    )

    parser.add_argument(
        '--logging_file',
        '-o',
        help='Full path to the log file'
    )

    parser.add_argument(
        '--logging_level',
        '-l',
        default='DEBUG',
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        help='Logggin level'
    )

    args = parser.parse_args()
    _config_logging(args.logging_level, args.logging_file)
    logger.info('Loading data')

    run()
